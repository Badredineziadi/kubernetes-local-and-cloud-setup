## Tools
- minikube
- kubeadm
- kops
- aws EKS

## Kubeadm for cluster(local setup)
- use vagrant to bring up a VM that will excute a shell script to install kubeadm, kubelet, kubectl and docker  
- install a network addon on each node
## Kops on aws

- create an EC2 instance for kops
- create an s3 bucket to store the state of kops
- create an I AM use and a hosted zone
- install kops and kubectl in the instance and create the cluster
## EKS on aws
- use a vagrantfile to bring up a VM that consumes some script to install awscli, kubectl and eksctl, then run the eks-cluster-setup script



